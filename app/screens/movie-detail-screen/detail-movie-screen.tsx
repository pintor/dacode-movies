import React, { FunctionComponent as Component } from "react"
import { Dimensions, ImageStyle, Text, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { Screen } from "../../components"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { Image } from "react-native-elements"

const ITEM_WIDTH = Dimensions.get("window").width
const SIZE = ITEM_WIDTH / 2 - 10
const HEIGHT = SIZE * 2 - 150

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: 0,
}

const IMAGE: ImageStyle = {
  width: "100%",
  height: HEIGHT,
}
const BOLD: TextStyle = { fontWeight: "bold" }

const TITLE_TEXT: TextStyle = {
  ...BOLD,
  paddingHorizontal: spacing[2],
  color: color.text,
  fontSize: 16,
}

const TEXT: TextStyle = {
  paddingHorizontal: spacing[2],
  color: color.text,
  fontSize: 16,
}
const TITLE: TextStyle = {
  ...TITLE_TEXT,
  paddingVertical: spacing[4],
  fontSize: 32,
}
const ITEM: ViewStyle = {
  paddingVertical: spacing[2],
  paddingHorizontal: spacing[2],
}

export const DetailMovieScreen: Component = observer(function DetailMovieScreen() {
  const rootStore = useStores()
  React.useEffect(() => {
    rootStore.getDetailMovie()
  }, [])

  return (
    <View style={FULL}>
      <Screen style={CONTAINER} preset="scroll" unsafe backgroundColor={color.palette.white}>
        <View>
          <Image
            resizeMode={"cover"}
            source={{ uri: rootStore.currentMovie.backdrop_path }}
            style={IMAGE}
          />
        </View>

        <View>
          <View>
            <Text style={TITLE}>{rootStore.detailMovie.title}</Text>
          </View>

          <View style={ITEM}>
            <Text style={TITLE_TEXT}>Duración:</Text>
            <Text style={TEXT}>{rootStore.detailMovie.runtime} min</Text>
          </View>

          <View style={ITEM}>
            <Text style={TITLE_TEXT}>Fecha de estreno:</Text>
            <Text style={TEXT}>{rootStore.detailMovie.release_date}</Text>
          </View>

          <View style={ITEM}>
            <Text style={TITLE_TEXT}>Calificación</Text>
            <Text style={TEXT}>{rootStore.detailMovie.vote_average}</Text>
          </View>

          <View style={ITEM}>
            <Text style={TITLE_TEXT}>Géneros:</Text>
            <Text style={TEXT}>
              {rootStore.detailMovie.genres.map(gender => gender.name + ", ")}
            </Text>
          </View>

          <View style={ITEM}>
            <Text style={TITLE_TEXT}>Descripción:</Text>
            <Text style={TEXT}>{rootStore.detailMovie.overview}</Text>
          </View>
        </View>
      </Screen>
    </View>
  )
})
