import React, { FunctionComponent as Component } from "react"
import {
  Dimensions,
  FlatList,
  ImageStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { observer } from "mobx-react-lite"
import { Screen } from "../../components"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { Card, Icon } from "react-native-elements"
import { useNavigation } from "@react-navigation/native"

const ITEM_WIDTH = Dimensions.get("window").width
const SIZE = ITEM_WIDTH / 2 - 10
const HEIGHT = SIZE * 2 - 95

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[0],
  marginTop: spacing[2],
}

const WRAPPER_IMAGE_CONTAINER: ImageStyle = {
  borderRadius: 10,
  overflow: "hidden",
  height: HEIGHT,
}

const CARD_RADIUS: ViewStyle = {
  borderRadius: 10,
  overflow: "hidden",
}

const CARD: ViewStyle = {
  ...CARD_RADIUS,
  borderWidth: 0,
  elevation: 0,
  paddingHorizontal: 0,
  marginHorizontal: 5,
  marginVertical: 5,
  backgroundColor: "red",
}

const OVERLAY: ViewStyle = {
  flex: 1,
  marginLeft: 5,
  borderBottomLeftRadius: 10,
  borderBottomRightRadius: 10,
  position: "absolute",
  left: 0,
  bottom: 0,
  opacity: 0.7,
  backgroundColor: "black",
  width: SIZE,
}

const BOLD: TextStyle = { fontWeight: "bold" }
const TEXT: TextStyle = {
  ...BOLD,
  paddingHorizontal: spacing[2],
  paddingVertical: spacing[1],
  color: "#CCC",
  fontSize: 16,
}

const OVERLAY_CONTAINER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
}

const VOTE_AVERAGE: ViewStyle = {
  flexDirection: "row",
}

export const MoviesScreen: Component = observer(function MoviesScreen() {
  const rootStore = useStores()
  React.useEffect(() => {
    rootStore.getMovies()
  }, [])

  const navigation = useNavigation()
  // const nextScreen = () => navigation.navigate("demo")

  const renderProducts = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          rootStore.setCurrentMovie(item)
          navigation.navigate("detail")
        }}
      >
        <Card
          wrapperStyle={{ width: SIZE, height: HEIGHT }}
          imageStyle={WRAPPER_IMAGE_CONTAINER}
          containerStyle={CARD}
          imageProps={{ borderRadius: 10, resizeMode: "cover" }}
          image={{ uri: item.poster_path }}
        />
        <View style={OVERLAY}>
          <View>
            <Text style={TEXT}>{item.title}</Text>
          </View>
          <View style={OVERLAY_CONTAINER}>
            <Text style={TEXT}>{item.release_date}</Text>
            <View style={VOTE_AVERAGE}>
              <Icon type={"material"} name="star-border" color="#BF9022" />
              <Text style={TEXT}>{item.vote_average}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View style={FULL}>
      <Screen style={CONTAINER} preset="fixed" unsafe backgroundColor={color.palette.white}>
        <FlatList
          numColumns={2}
          showsVerticalScrollIndicator={false}
          data={rootStore.movies}
          renderItem={renderProducts}
          keyExtractor={(item, index) => index.toString()}
        />
      </Screen>
    </View>
  )
})
