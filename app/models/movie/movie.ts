import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const MovieModel = types
  .model("Movie")
  .props({
    id: types.identifierNumber,
    title: types.string,
    poster_path: types.string,
    release_date: types.string,
    vote_average: types.number,
    backdrop_path: types.string,
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type MovieType = Instance<typeof MovieModel>
export interface Movie extends MovieType {}
type MovieSnapshotType = SnapshotOut<typeof MovieModel>
export interface MovieSnapshot extends MovieSnapshotType {}
