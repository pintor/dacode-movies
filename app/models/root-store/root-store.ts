import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Movie, MovieModel } from "../movie/movie"
import { withEnvironment } from "../extensions/with-environment"
import { MovieDetailModel } from "../movie-detail/movie-detail"

/**
 * A RootStore model.
 */
export const RootStoreModel = types
  .model("RootStore")
  .props({
    baseUrl: types.union(types.undefined, types.string),
    movies: types.optional(types.array(MovieModel), []),
    currentMovie: types.maybe(types.reference(MovieModel)),
    detailMovie: types.maybe(MovieDetailModel),
  })
  .extend(withEnvironment)
  .actions(self => ({
    afterCreate: flow(function*() {
      const result = yield self.environment.api.getConfiguration()
      self.baseUrl = result.response.images.base_url
    }),
    getMovies: flow(function*() {
      const result = yield self.environment.api.getMoviesTheatres(1)
      result.response.results.map(r => {
        r.poster_path = `${self.baseUrl}/w500/${r.poster_path}`
        r.backdrop_path = `${self.baseUrl}/w500/${r.backdrop_path}`
      })
      self.movies = result.response.results
    }),
    getDetailMovie: flow(function*() {
      const result = yield self.environment.api.getDetailMovie(self.currentMovie.id)
      self.detailMovie = result.response
    }),
    setCurrentMovie(movie: Movie) {
      self.currentMovie = movie
    },
  }))

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
