import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const GenresModel = types
  .model("Genres")
  .props({
    id: types.identifierNumber,
    name: types.string,
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type GenresType = Instance<typeof GenresModel>
export interface Genres extends GenresType {}
type GenresSnapshotType = SnapshotOut<typeof GenresModel>
export interface GenresSnapshot extends GenresSnapshotType {}
