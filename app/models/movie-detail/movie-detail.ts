import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { GenresModel } from "../genres/genres"

/**
 * Model description here for TypeScript hints.
 */
export const MovieDetailModel = types
  .model("MovieDetail")
  .props({
    backdrop_path: types.string,
    title: types.string,
    runtime: types.number,
    release_date: types.string,
    vote_average: types.number,
    genres: types.optional(types.array(GenresModel), []),
    overview: types.string,
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type MovieDetailType = Instance<typeof MovieDetailModel>
export interface MovieDetail extends MovieDetailType {}
type MovieDetailSnapshotType = SnapshotOut<typeof MovieDetailModel>
export interface MovieDetailSnapshot extends MovieDetailSnapshotType {}
